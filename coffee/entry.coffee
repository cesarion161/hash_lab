fs = require 'fs'
crypto = require 'crypto'
randString = require 'randomstring'
hashLength = 4
getHash = (str) ->
	hash = crypto.createHash('md4').update(str).digest('hex').slice 0,hashLength
collide = (hashOrigin) ->
	until hashOrigin is hash
		str = do randString.generate
		hash = getHash str
		console.log hashOrigin, hash
	str

origin = __dirname + '/origin.txt'
file = __dirname + '/file.txt'

strOrigin = fs.readFileSync origin, 'utf8'
hashOrigin = getHash strOrigin

newStr = collide hashOrigin
fs.writeFileSync file, newStr
console.log strOrigin + '\t' + hashOrigin
console.log newStr + '\t' + getHash newStr


