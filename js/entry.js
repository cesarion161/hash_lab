(function() {
  var collide, crypto, file, fs, getHash, hashLength, hashOrigin, newStr, origin, randString, strOrigin;

  fs = require('fs');

  crypto = require('crypto');

  randString = require('randomstring');

  hashLength = 4;

  getHash = function(str) {
    var hash;
    return hash = crypto.createHash('md4').update(str).digest('hex').slice(0, hashLength);
  };

  collide = function(hashOrigin) {
    var hash, str;
    while (hashOrigin !== hash) {
      str = randString.generate();
      hash = getHash(str);
      console.log(hashOrigin, hash);
    }
    return str;
  };

  origin = __dirname + '/origin.txt';

  file = __dirname + '/file.txt';

  strOrigin = fs.readFileSync(origin, 'utf8');

  hashOrigin = getHash(strOrigin);

  newStr = collide(hashOrigin);

  fs.writeFileSync(file, newStr);

  console.log(strOrigin + '\t' + hashOrigin);

  console.log(newStr + '\t' + getHash(newStr));

}).call(this);
